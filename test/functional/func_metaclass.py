# pylint: disable=missing-docstring,missing-module-attribute,invalid-name,blacklisted-name,too-few-public-methods,super-init-not-called,old-style-class,no-init

import abc

class meta(type):
    pass

__metaclass__ = abc.ABCMeta

class C(object):
    pass

class D(object):  # [safepython-metaclass]
    __metaclass__ = abc.ABCMeta

class A:  # [safepython-metaclass]
    pass

__metaclass__ = meta

class B:  # [safepython-metaclass]
    pass

__metaclass__ = type

class F:
    pass
