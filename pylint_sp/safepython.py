# Copyright (c) 2014 LOGILAB S.A. (Paris, FRANCE).
# http://www.logilab.fr/ -- mailto:contact@logilab.fr
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 2 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, see <http://www.gnu.org/licenses/>.
"""safepython pylint checker
"""

from astroid.scoped_nodes import builtin_lookup
from pylint.interfaces import IAstroidChecker
from pylint.checkers import BaseChecker
from pylint.checkers import utils

class SafePythonChecker(BaseChecker):
    """Checks for constructs discouraged by the safepython project
    """

    __implements__ = (IAstroidChecker,)

    name = 'safepython'
    msgs = {
            'W5101': ('Use of the %s builtin',
                'safepython-using-*attr',
                'Changing the list of instance of class variables dynamically should be avoided'
                ),
            'W5102': ('Implements %s method',
                'safepython-reserved-method',
                'Overriding reserved methods can be dangerous and/or confusing'
                ),
            'C5103': ('Use of the type() constructor',
                'safepython-type-constructor',
                'Using type() to create a class makes static analysis harder'
                ),
            'E5104': ('Using type() with %d arguments',
                'safepython-wrong-type-args',
                'Calling type() with the wrong number of arguments'
                ),
            'C5105': ('Using type() instead of isinstance()',
                'safepython-type',
                'Consider using isinstance() instead of type()'
                ),
            'W5106': ('Overriding the default mro',
                'safepython-mro-override',
                'Overriding the default method resolution order risks defeating static analysis'
                ),
            'W5107': ('Class inherits from non-mixins %s',
                'safepython-multiple-inheritance',
                'Used when a class inherits from more than one non-mixin class.'),
            'W5108': ('Class uses a metaclass',
                'safepython-metaclass',
                'Used when the __metaclass__ attribute is used in a class definition.'),
            }

    def visit_callfunc(self, node):
        funcs = tuple(builtin_lookup(f)[1][0] for f in ('getattr', 'setattr', 'delattr', 'dir'))
        if utils.safe_infer(node.func) in funcs:
            self.add_message('safepython-using-*attr', node=node.func, args=node.func.name)
        if utils.safe_infer(node.func) == builtin_lookup('type')[1][0]:
            if len(node.args) == 3:
                self.add_message('safepython-type-constructor', node=node)
            elif len(node.args) != 1:
                self.add_message('safepython-wrong-type-args', node=node, args=len(node.args))
            else:
                # XXX restrict to use as part of a boolean expr?
                self.add_message('safepython-type', node=node)


    def visit_function(self, node):
        if not node.is_method():
            return
        if node.name in ('__getattr__', '__setattr__', '__dir__'):
            self.add_message('safepython-reserved-method', node=node, args=node.name)

    def visit_class(self, node):
        if node.type == 'metaclass':
            if 'mro' in node:
                self.add_message('safepython-mro-override', node=node)
        self._check_multiple_inheritance(node)
        self._check_metaclass(node)

    def _check_metaclass(self, node):
        metaclass = node.metaclass()
        if metaclass is not None and metaclass != builtin_lookup('type')[1][0]:
            self.add_message('safepython-metaclass', node=node)

    def _check_multiple_inheritance(self, node):
        init_parents = []
        for parent in node.ancestors(recurs=False):
            try:
                init = parent.igetattr('__init__').next()
            except astroid.InferenceError:
                init_parents.append(parent.name)
            else:
                if init.parent != builtin_lookup('object')[1][0]:
                    init_parents.append(parent.name)
        if len(init_parents) > 1:
            self.add_message('safepython-multiple-inheritance', args=init_parents, node=node)


def register(linter):
    linter.register_checker(SafePythonChecker(linter))
