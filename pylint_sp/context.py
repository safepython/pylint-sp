
from astroid import InferenceError, nodes
from pylint.interfaces import IAstroidChecker
from pylint.checkers import BaseChecker, utils
from pylint.checkers.stdlib import OPEN_MODULE

MSGS = {
    'W4201': ('file should be opened as a context manager',
              'file-without-context-manager',
              'Used when a file is opened outside of a context manager'),
    'W4202': ('lock should be used as a context manager',
              'lock-without-context-manager',
              'Used when a lock is acquired/released without context manager')
    }


class ContextChecker(BaseChecker):

    __implements__ = (IAstroidChecker,)

    name = 'context-manager'
    msgs = MSGS
    priority = -1

    def visit_callfunc(self, node):
        if not hasattr(node, 'func'):
            return
        if isinstance(node.parent, nodes.With):
            return
        infer = utils.safe_infer(node.func)
        if not infer:
            return
        if infer.root().name == OPEN_MODULE:
            if getattr(node.func, 'name', None) in ('open', 'file'):
                self.add_message('file-without-context-manager', node=node)
        elif isinstance(node.func, nodes.Getattr) and node.func.attrname in ('acquire', 'release'):
            self.add_message('lock-without-context-manager', node=node)
                

def register(linter):
    linter.register_checker(ContextChecker(linter))
